## Joint Probe Trajectory Controller ##

Controller for executing joint-space trajectories on a group of joints, that can be interrupted in realtime based on sensor / probe feedback.

Based on the Joint Trajectory Controller, see the [ROS wiki page](http://wiki.ros.org/joint_trajectory_controller).

